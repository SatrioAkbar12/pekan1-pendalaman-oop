<?php

    require_once('Hewan.php');
    require_once('Fight.php');

    class Elang extends Hewan{
        use Fight;

        protected $jumlahKaki = 2;
        protected $keahlian = "terbang tinggi";

        public function __construct($nama)
        {
            $this->nama = $nama;
            $this->attackPower = 10;
            $this->defencePower = 5;
        }

        public function getInfoHewan(){
            echo "<strong>Info Hewan:</strong><br>";
            echo "Jenis hewan: Elang<br>";
            echo "Nama: $this->nama<br>";
            echo "Darah: $this->darah<br>";
            echo "Jumlah kaki: $this->jumlahKaki<br>";
            echo "Keahlian: $this->keahlian<br>";
            echo "Attack Power: $this->attackPower<br>";
            echo "Defence Power: $this->defencePower<br>";
        }
    }

    class Harimau extends Hewan{
        use Fight;

        protected $jumlahKaki = 4;
        protected $keahlian = "lari cepat";

        public function __construct($nama)
        {
            $this->nama = $nama;
            $this->attackPower = 7;
            $this->defencePower = 8;
        }

        public function getInfoHewan(){
            echo "<strong>Info Hewan:</strong><br>";
            echo "Jenis hewan: Harimau<br>";
            echo "Nama: $this->nama<br>";
            echo "Darah: $this->darah<br>";
            echo "Jumlah kaki: $this->jumlahKaki<br>";
            echo "Keahlian: $this->keahlian<br>";
            echo "Attack Power: $this->attackPower<br>";
            echo "Defence Power: $this->defencePower<br>";
        }
    }

    $elang = new Elang("elang_1");
    $harimau = new Harimau("harimau_1");

    echo $elang->atraksi();
    echo "<br>";
    echo $elang->serang($harimau);
    echo "<br>";
    echo $elang->diserang($harimau);
    echo "<br><br>";

    $elang->getInfoHewan();
    echo "<br>";
    $harimau->getInfoHewan();

?>