<?php

    trait Fight{
        public $attackPower;
        public $defencePower;

        public function __construct($attack, $defence){
            $this->attackPower = $attack;
            $this->defencePower = $defence;
        }

        public function serang($musuh){
            return "$this->nama sedang menyerang $musuh->nama";
        }

        public function diserang($musuh){
            $this->darah = $this->darah - ($musuh->attackPower / $this->defencePower);

            return "$this->nama sedang diserang";
        }
    }

?>